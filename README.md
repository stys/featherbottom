##Featherbottom##

Simple Node.js server for static web site and minimal API to load and save json files.

``` text
# Directory structure of featherbottom application:

myapp
|
+- data/ # put .json files here
|
+- site/ # put any static web site here
	|
	+- index.html
	|
	...
```

### Install dependencies 
run `npm install` from root directory

### Start server: 
run `node path/to/featherbottom.js` from root directory of your application

### Load web application
``` text
GET    http://localhost:9090/site
```

### Load some json file: 
``` text
GET    http://localhost:9090/data?file=mydata.json
```

### Save some json to file: 
``` text
POST    http://localhost:9090/data?file=mydata2.json 
Content-Type: application/json
Request Body: json data to be saved in file
```