/* FEATHERBOTTOM - very simple server for static content and data stored in json files */
/* Usefull for creating demo ajax applications */

/* TODO: 
[1] support subfolders in data/ directory
[2] pass port from command line args
/*

/* Load file system module */
var fs = require('fs');

/* Load express module */
var express = require('express');

/* URL parser module */
var url = require('url');

/* Lockfile module */
var lockFile = require('lockfile');

/* Create new express application instance */
var app = express();

/* Config */
var port = 9090; // port
var workdir = process.cwd(); // works in directory where it has been started
app.use(express.json()); // support JSON-encoded bodies in POST requests

/* Serve static site */
app.use('/site', express.static(workdir + "/site"));

/* Get json data from given location /data?file=path/to/file.json */
app.get("/data", function(request, response) {
	
	/* Parse url */
	var parts = url.parse(request.url, true);
	/* Get file argument */
	var file = parts.query["file"]; 
	
	if( !file ) {
		response.send("Incorrect request: file not specified", 400);
	} else {
		/* FIXME: if request contains multiple file= statements, for example
		    http://localhost:9090/data?file=mydata.json&file=mydata2.json
		    In this case file will not be just a simple string, but an array of string.
		    Code below will fail.
		*/
				
		/* Full filename */
		var f = workdir + "/data/" + file;
		
		/* Lock file before access */
		lockFile.lock(f + '.lock', function(lockerr) {
			
			/* Check for lock error before doing anything else */
			if( lockerr ) {
				/* File is locked by somebody else... */
				response.send(lockerr, 400);
				return;
			}
			
			/* Try to load contents of the file as json */
			fs.readFile(f, "utf8", function(readerr, data) {
				
				/* Check results of read opperation */
				if (readerr) {
					response.send(readerr, 400); 
				} else {
					/* TODO: handle parse errors */
					response.send(JSON.parse(data), 200);
				}
				
				/* Unlock the file */
				lockFile.unlock(f + '.lock', function(unlockerr) {
					if( unlockerr ) {
						/* There was error while unlocking the file. This is bad... */
						console.log(unlockerr);
					}
				});
			});
		});
	}
});

/* Put data into json file */
app.post("/data", function(request, response) {
	
	/* Parse url */
	var parts = url.parse(request.url, true);
	/* Get file argument */
	var file = parts.query["file"];
	
	if( !file ) {
		/* TODO: report error in JSON format */
		response.send("Incorrect request: file not specified", 400);
	} else {
		/* Full filename */
		var f = workdir + "/data/" + file;		
		
		/* Lock file before access */
		lockFile.lock(f + ".lock", function(lockerr) {
			
			/* Check file lock */
			if (lockerr) {
				/* File is locked by somebody else */
				response.send(lockerr, 400);
				return;
			}
								
			/* Write body to file */
			fs.writeFile(f, JSON.stringify(request.body, null, 4), function(writerr) {
				
				/* Check results of write operation */
				if (writerr) {
					response.send(writerr, 400);
				} else {
					response.send(200);
				}
				
				/* Unlock file */
				lockFile.unlock(f + '.lock', function(unlockerr) {
					if (unlockerr) {
						/* There was error while unlocking the file. This is bad... */
						console.log(unlockerr);
					}
				});
			});
		});			
	}
});

/* Start server */
app.listen(port, function(){
	console.log("Featherbottom server started on port %d", port);
});
